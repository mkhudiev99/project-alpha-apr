from django.urls import path, reverse_lazy
from django.views.generic.base import RedirectView
from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("", RedirectView.as_view(url=reverse_lazy("list_projects"))),
]
