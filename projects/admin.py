from django.contrib import admin

# Register your models here.
from projects.models import Project
from tasks.models import Task


class TaskAdmin(admin.ModelAdmin):
    pass


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Project, ProjectAdmin)

admin.site.register(Task, TaskAdmin)
